﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPortal : MonoBehaviour
{
    
    public Sprite brokenPortal;
    public GameObject enemyPrefab;
    
    public float spawnRate = 1f;

    private SpriteRenderer myRenderer;
    private float nextSpawn;
    private float health = 200f;
    private float spawnRange = 5f;
    private bool isAlive;
    private Vector2 dif2Player;

    private void Start()
    {
        isAlive = true;
        myRenderer = GetComponent<SpriteRenderer>();
    }
    void Update()
    {
        dif2Player = transform.position - Player.Instance.transform.position;
        if (health <= 0)
        {
            isAlive = false;
            myRenderer.sprite = brokenPortal;
            Destroy(GetComponent<BoxCollider2D>());
        }
        if (dif2Player.x < spawnRange) // IF PLAYER IS CLOSE ENOUGH TO PORTAL
        { 
            if (Time.time > nextSpawn && isAlive) // IF PORTAL IS FINE AND COOLDOWN IS READY
            {
                Instantiate(enemyPrefab, transform.position, Quaternion.identity);
                nextSpawn = Time.time + spawnRate;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet" && isAlive)
        {
            health -= Player.Instance.Damage;
        }
    }
}
