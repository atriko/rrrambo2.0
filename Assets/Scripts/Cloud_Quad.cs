﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud_Quad : MonoBehaviour
{
    private float cloudSpeed = 0.2f;
    Renderer myRenderer;
    // Start is called before the first frame update
    void Start()
    {
        myRenderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        myRenderer.material.mainTextureOffset += new Vector2(-cloudSpeed * Time.deltaTime, 0f);
    }
}
