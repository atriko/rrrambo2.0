﻿using System;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    
    private float maxHealth = 100f;
    private float curHealth;
    private float healthPct;

    public event Action<float> PlayerHit = delegate { }; 

    // Start is called before the first frame update
    void Start()
    {
        curHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (curHealth <= 0)
        {
            Player.Instance.Die();
            Player.Instance.IsAlive = false;
        }
    }
    public void Hit()
    {
        AudioManager.Instance.Play("PlayerHurt");
        curHealth -= Enemy.Instance.Damage;
        healthPct = curHealth / maxHealth;
        PlayerHit(healthPct);
    }
}
