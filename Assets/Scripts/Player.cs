﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Singleton
    private static Player instance;

    public static Player Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Player>();
            }
            return instance;
        }
    }
    #endregion

    Rigidbody2D myRigidbody;
    Animator myAnimator;

    private Vector3 mousePos;
    private float playeraimX;
    private float playeraimY;

    public GameObject bulletPrefab;
    
    //VARIABLES
    private float movementSpeed = 2f; //2f
    private float jumpForce = 3.5f; //3.5f
    private float damage = 10f; //10f
    private float maxAmmo = 30f; //30f
    private float reloadTime = 1f; // 1f in seconds
    private float fireRate = 0.1f; // every this* seconds
    private float nextFire = 0f; // always zero
    private float currentAmmo;
    
    public float Damage { get => damage; private set { } }
    public float CurrentAmmo { get => currentAmmo; private set { } }
    public bool IsAlive { get => isAlive; set { value = isAlive; } }

    //PLAYER REFERENCE POINTS
    public Transform gCheck, gCheckR, gCheckL, firePoint, firePointUp, firePointUpDiag, firePointDownDiag, firePointCrouch;
    
    private bool onGround;
    private bool isCrouching;
    private bool facingRight;
    private bool isReloading;
    private bool isAlive;


    // Start is called before the first frame update
    void Start()
    {
        isAlive = true;
        facingRight = true;
        currentAmmo = maxAmmo;
        myAnimator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleInputs();
        
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        HandleMovement(horizontal);
    }

    private void HandleMovement(float horizontal)
    {
        if (isAlive)
        {
            if ((Physics2D.Linecast(transform.position, gCheck.position, 1 << LayerMask.NameToLayer("Ground"))) ||
            (Physics2D.Linecast(transform.position, gCheckR.position, 1 << LayerMask.NameToLayer("Ground"))) ||
            (Physics2D.Linecast(transform.position, gCheckL.position, 1 << LayerMask.NameToLayer("Ground"))))
            {
                myAnimator.SetBool("jumping", false);
                onGround = true;
            }
            else
            {
                myAnimator.SetBool("jumping", true);
                onGround = false;
            }
            if (!isCrouching)
            {
                myRigidbody.velocity = new Vector2(horizontal * movementSpeed, myRigidbody.velocity.y);
            }
            myAnimator.SetFloat("speed", Mathf.Abs(horizontal));
        }
    }

    private void CheckDirection(float mouseposx)
    {
        if (mouseposx > 0 && !facingRight || mouseposx < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }

    }

    private void HandleInputs()
    {
        if (isAlive)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            playeraimX = mousePos.x - transform.position.x;
            playeraimY = mousePos.y - transform.position.y;


            myAnimator.SetFloat("MouseX", playeraimX);
            myAnimator.SetFloat("MouseY", playeraimY);
            CheckDirection(playeraimX);

            if (Input.GetMouseButton(0))
            {
                if (currentAmmo > 0 && !isReloading)
                {
                    float angle = Mathf.Atan2(playeraimY, playeraimX) * Mathf.Rad2Deg;
                    Fire(angle);
                    myAnimator.SetBool("shooting", true);
                }
                else if (!isReloading)
                {
                    AudioManager.Instance.Play("Reload");
                    myAnimator.SetBool("shooting", false);
                    StartCoroutine(Reload(reloadTime));
                }
            }
            else
            {
                myAnimator.SetBool("shooting", false);
            }

            if (Input.GetKey(KeyCode.W))
            {
                if (onGround)
                {
                    myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);

                }
            }
            if (Input.GetKey(KeyCode.LeftControl) && onGround)
            {
                myRigidbody.velocity = new Vector2(0, 0);
                myAnimator.SetBool("crouching", true);
                isCrouching = true;
            }
            else
            {
                myAnimator.SetBool("crouching", false);
                isCrouching = false;
            }
        }

    }

    IEnumerator Reload(float reloadTime)
    {
        isReloading = true;
        yield return new WaitForSeconds(reloadTime);
        currentAmmo = maxAmmo;
        isReloading = false;
    }

    private void Fire(float angle)
    {
        if (Time.time > nextFire)
        {
            if (isCrouching)
            {
                if (facingRight)
                    FireBullet(firePointCrouch, Quaternion.Euler(0, 0, -180), Vector2.right);
                else
                    FireBullet(firePointCrouch, Quaternion.Euler(0, 0, 180), Vector2.left);

            }
            else
            {
                if (angle > 26.5f && angle < 63.5f || angle > 116.5f && angle < 153.5f)//UPDIAG
                {
                    if (facingRight)
                        FireBullet(firePointUpDiag, Quaternion.Euler(0, 0, -45), new Vector2(1, 1));
                    else
                        FireBullet(firePointUpDiag, Quaternion.Euler(0, 0, 45), new Vector2(-1, 1));
                }
                else if (angle < 26.5f && angle > -26.5f || angle > 153.5f || angle < -153.5f)//NORMAL
                {
                    if (facingRight)
                        FireBullet(firePoint, Quaternion.Euler(0, 0, -180), Vector2.right);
                    else
                        FireBullet(firePoint, Quaternion.Euler(0, 0, 180), Vector2.left);
                }
                else if (angle < 116.5f && angle > 57.5f)//UP
                {
                    if (facingRight)
                        FireBullet(firePointUp, Quaternion.Euler(0, 0, -90), Vector2.up);
                    else
                        FireBullet(firePointUp, Quaternion.Euler(0, 0, 90), Vector2.up);
                }
                else if (angle < -26.5f && angle > -153.5f)//DOWNDIAG
                {
                    if (facingRight)
                        FireBullet(firePointDownDiag, Quaternion.Euler(0, 0, -225), new Vector2(1, -1));
                    else
                        FireBullet(firePointDownDiag, Quaternion.Euler(0, 0, 225), new Vector2(-1, -1));
                }
                
            }
            currentAmmo--;
            nextFire = Time.time + fireRate;
        }
    }

    private void FireBullet(Transform firePoint, Quaternion rotation, Vector2 direction)
    {
        GameObject tmp = Instantiate(bulletPrefab, firePoint.position, rotation);
        tmp.GetComponent<Bullet>().Initiliaze(direction);
    }

    //private IEnumerator Knockback(float duration, float power, Vector2 direction)
    //{
    //    float timer = 0;
    //    while (duration > timer)
    //    {
    //        timer += Time.deltaTime;
    //        myRigidbody.AddForce(new Vector2(direction.x * -power, direction.y * power));
    //    }
    //    yield return 0;
    //}
    public void Die()
    {
        myRigidbody.velocity = Vector2.zero;
        isAlive = false;
        myAnimator.SetBool("dead", true);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            //KNOCKBACK WILL BE ADDED

            //IMMORTALITY WILL BE ADDED
            AudioManager.Instance.Play("PlayerHurt");
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "EnemyBullet")
        {
            FindObjectOfType<PlayerHealth>().Hit();
        }
    }
}
