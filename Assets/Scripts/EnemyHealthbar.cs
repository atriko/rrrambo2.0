﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthbar : MonoBehaviour
{
    public Image healthbar;
    // Start is called before the first frame update
    void Awake()
    {
        GetComponentInParent<Enemy>().OnHit += UpdateHealthBar;
    }

    private void UpdateHealthBar(float healthpct)
    {
        if (healthpct <= 0)
        {
            gameObject.SetActive(false);
            Destroy(gameObject.GetComponentInParent<CapsuleCollider2D>());
            Destroy(gameObject.GetComponentInParent<Rigidbody2D>());

        }
        healthbar.fillAmount = healthpct;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
