﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    private float smoothness = 0.2f;

    // Update is called once per frame
    void FixedUpdate()
    {
        float camx = target.position.x;
        float camy = target.position.y;
        if (camx < 0f)
        {
            camx = 0f;
        }
        if (camx >26f)
        {
            camx = 26f;
        }
        Vector3 camPosition = new Vector3(camx, camy + 0.5f, -10f);

        Vector3 camWithSmooth = Vector3.Lerp(transform.position, camPosition, smoothness);
        transform.position = camWithSmooth;
        
    }
}
