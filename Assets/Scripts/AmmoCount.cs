﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class AmmoCount : MonoBehaviour
{
    public TextMeshProUGUI ammotext;

    // Update is called once per frame
    void Update()
    {
        ammotext.text = "x" + Player.Instance.CurrentAmmo.ToString();
    }
}
