﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    private float movespeed = 4f;
    private float animationspeed = 1.5f;

    Vector2 direction;

    Rigidbody2D bullet;
    Animator myAnimator;

    public GameObject bulletImpactPrefab;


    private void Awake()
    {
        //AudioManager.Instance.Play("Fire");
    }
    void Start()
    {
        bullet = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myAnimator.speed = animationspeed;

        bullet.velocity = direction * movespeed;

    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, myAnimator.GetCurrentAnimatorStateInfo(0).length);
    }
    public void Initiliaze(Vector2 direction)
    {
        this.direction = direction;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
    }
}
