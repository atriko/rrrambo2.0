﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthbar : MonoBehaviour
{
    private Image healthbar;

    void Awake()
    {
        FindObjectOfType<PlayerHealth>().PlayerHit += UpdateHealthBar;
        healthbar = GetComponentInChildren<Image>();
    }

    private void UpdateHealthBar(float healthpct)
    {
        healthbar.fillAmount = healthpct;
    }
}
