﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    private static Enemy instance;
    public static Enemy Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Enemy>();
            }
            return instance;
        }
    }
    public Transform firePointEnemyLeft;
    public Transform firePointEnemyRight;
    public GameObject enemyBulletPrefab;

    private float maxHealth=100f;
    private float curHealth;
    private float healthPct;
    private float damage = 5f;
    private float shootRange = 2f;
    private Vector2 dif2Player;

    public float Damage { get => damage; private set { } }

    //SOUND PLAY ONCE BOOL NEEDS WORK
    bool playOnce = true;

    public event Action<float> OnHit = delegate { };

    SpriteRenderer myRenderer;
    Animator myAnimator;
    private bool facingRight;
    private float nextFire;
    private float fireRate = 0.5f;

    void Start()
    {
        facingRight = false;
        curHealth = maxHealth;
        myAnimator = GetComponent<Animator>();
        myRenderer = GetComponent<SpriteRenderer>();
    }
    
    void FixedUpdate()
    {
        if (Player.Instance.IsAlive == false)
        {
            myAnimator.SetBool("run", false);
            myAnimator.SetBool("shoot", false);
            return;
        }
        if (curHealth <= 0 )
        {
            //SHITTY WORKAROUND FOR PLAYING THE DEATH SOUND ONCE IN UPDATE
             if (playOnce == true)
            {
                playOnce = false;
                AudioManager.Instance.Play("EnemyDeath");
            }
            myAnimator.SetBool("dead",true);
            Destroy(gameObject, myAnimator.GetCurrentAnimatorClipInfo(0).Length);
        }
        CheckDirection();
        if (Math.Abs(dif2Player.x) < shootRange)
        {
            myAnimator.SetBool("shoot", true);
            myAnimator.SetBool("run", false);
            Fire();
        }
        else
        {
            //RUN TO PLAYER
            transform.position = Vector2.MoveTowards(transform.position, Player.Instance.transform.position, 0.02f);
            myAnimator.SetBool("run", true);
            myAnimator.SetBool("shoot", false);

        }
    }

    private void Fire()
    {
        if (Time.time > nextFire)
        {
            if (facingRight)
                FireEnemyBullet(firePointEnemyRight, Quaternion.Euler(0, 0, -180), Vector2.right);
            else
                FireEnemyBullet(firePointEnemyLeft, Quaternion.Euler(0, 0, 180), Vector2.left);
            nextFire = Time.time + fireRate;
        }
    }

    private void FireEnemyBullet(Transform firePointEnemy, Quaternion rotation, Vector2 direction)
    {
        GameObject tmp = Instantiate(enemyBulletPrefab, firePointEnemy.position, rotation);
        tmp.GetComponent<EnemyBullet>().Initiliaze(direction);
    }

    private void CheckDirection()
    {
        dif2Player = transform.position - Player.Instance.transform.position;
        if (dif2Player.x > 0 && facingRight || dif2Player.x < 0 && !facingRight)
        {
            facingRight = !facingRight;
            myRenderer.flipX = !myRenderer.flipX;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag == "Bullet")
        {
            curHealth -= Player.Instance.Damage;
            healthPct = curHealth / maxHealth;
            OnHit(healthPct);
        }

    }
}
