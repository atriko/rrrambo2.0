﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletImpact : MonoBehaviour
{
    Animator myAnimator;
    // Start is called before the first frame update
    void Start()
    {
        myAnimator = GetComponent<Animator>();
        myAnimator.speed = 2f;
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, myAnimator.GetCurrentAnimatorStateInfo(0).length);
    }
}
