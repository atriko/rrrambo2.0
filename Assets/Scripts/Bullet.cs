﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float movespeed = 8f;
    private float animationspeed = 1.5f;

    Vector2 direction;
    
    Rigidbody2D bullet;
    Animator myAnimator;

    public GameObject bulletImpactPrefab;


    private void Awake()
    {
        AudioManager.Instance.Play("Fire");
    }
    // Start is called before the first frame update
    void Start()
    {
        bullet = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myAnimator.speed = animationspeed;

        bullet.velocity = direction * movespeed;

    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, myAnimator.GetCurrentAnimatorStateInfo(0).length);
    }
    public void Initiliaze(Vector2 direction)
    {
        this.direction = direction;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Instantiate(bulletImpactPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
